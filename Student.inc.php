<?php
/**
 * Drop-off & Pickup Student Info tab
 *
 * @package Student Pickup module
 */

require_once 'modules/Student_Pickup/includes/PickupDropoff.fnc.php';

// Set start date.
$start_date = RequestedDate( 'start', DBDate(), 'set' );

// Set end date.
$end_date = RequestedDate( 'end', DBDate(), 'set' );

if ( ! empty( $_REQUEST['pickup_dates'] ) )
{
	// We changed the date range. Do NOT save values.
	$_REQUEST['values'] = false;
}

// Action hook before update for Premium.
do_action( 'Student_Pickup/Student.inc.php|before_update' );

if ( AllowEdit()
	&& ! empty( $_REQUEST['values'] ) )
{
	foreach ( $_REQUEST['values'] as $id_or_date => $columns )
	{
		if ( ! VerifyDate( $id_or_date ) )
		{
			$id = $id_or_date;

			$sql = "UPDATE student_pickup SET ";

			foreach ( (array) $columns as $column => $value )
			{
				$sql .= DBEscapeIdentifier( $column ) . "='" . $value . "',";
			}

			$sql = mb_substr( $sql, 0, -1 ) . " WHERE ID='" . (int) $id . "'";

			DBQuery( $sql );
		}
		// New: check for Drop-off or Pickup time.
		elseif ( $columns['DROPOFF_TIME']
			|| $columns['PICKUP_TIME'] )
		{
			$sql = "INSERT INTO student_pickup ";

			$school_date = $id_or_date;

			$fields = 'SCHOOL_DATE,STUDENT_ID,';
			$values = "'" . $school_date . "','" . UserStudentID() . "',";

			$go = false;

			foreach ( (array) $columns as $column => $value )
			{
				if ( ! empty( $value )
					|| $value === '0' )
				{
					$fields .= DBEscapeIdentifier( $column ) . ',';
					$values .= "'" . $value . "',";
					$go = true;
				}
			}
			$sql .= '(' . mb_substr( $fields, 0, -1 ) . ') values(' . mb_substr( $values, 0, -1 ) . ')';

			if ( $go )
			{
				DBQuery( $sql );
			}
		}
	}

	RedirectURL( 'values' );
}

if ( ! $_REQUEST['modfunc'] )
{
	// Display Student Photo, Name, Grade Level and QR code with link to this same Student Info tab.
	echo '<table class="width-100p valign-top fixed-col"><tr class="st"><td>';

	// @since 9.0 Fix Improper Access Control security issue: add random string to photo file name.
	$picture_path = (array) glob( $StudentPicturesPath . '*/' . UserStudentID() . '.*jpg' );

	$picture_path = end( $picture_path );

	if ( $picture_path ):
	?>
		<img src="<?php echo URLEscape( $picture_path ); ?>" class="user-photo" alt="<?php echo AttrEscape( _( 'Student Photo' ) ); ?>" />
		</td><td>
	<?php endif;

	$grade_level = DBGetOne( "SELECT TITLE
		FROM student_enrollment se,school_gradelevels sg
		WHERE STUDENT_ID='" . UserStudentID() . "'
		AND se.SCHOOL_ID='" . UserSchool() . "'
		AND sg.SCHOOL_ID=se.SCHOOL_ID
		AND SYEAR='" . UserSyear() . "'
		AND START_DATE<=CURRENT_DATE
		AND (END_DATE IS NULL OR END_DATE>=CURRENT_DATE)
		AND GRADE_ID=sg.ID" );

	echo NoInput(
		$student['FULL_NAME'],
		$grade_level
	);

	echo '</td><td>';

	$qr_code_image_url = StudentPickupQRCodeImageURL( UserStudentID(), $_REQUEST['category_id'] );

	?>
	<img src="<?php echo URLEscape( $qr_code_image_url ); ?>" class="user-photo" alt="<?php echo AttrEscape( dgettext( 'Student_Pickup', 'QR code' ) ); ?>" />
	<?php

	// Action hook to add extra info or fields (Premium).
	do_action( 'Student_Pickup/Student.inc.php|extra_info' );

	echo '</td></tr></table>';

	// Do not Print list via Print Student Info program.
	if ( $_REQUEST['modname'] !== 'Students/PrintStudentInfo.php' )
	{
		// Fix CSS responsive List width: do NOT use the .fixed-col class, use pure CSS.
		echo '<table class="width-100p valign-top" style="table-layout: fixed;"><tr><td>';

		DrawHeader(
			PrepareDate( $start_date, '_start', false ) . ' &nbsp; ' . _( 'to' ) . ' &nbsp; ' .
			PrepareDate( $end_date, '_end', false ) . ' ' .
			'<input type="submit" name="pickup_dates" value="' . AttrEscape( _( 'Go' ) ) .
				// Remove modfunc=update as we only change date range here.
				'" onclick="this.form.action=this.form.action.replace(\'&modfunc=update\', \'\');console.log(this.form.action);" />'
		);

		// Display Student Drop-off & Pickup list.
		StudentPickupListOutput( UserStudentID(), $start_date, $end_date );

		echo '</td></tr></table>';
	}
}
