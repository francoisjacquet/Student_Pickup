<?php
/**
 * Generate a 300x300 black & white QR code image
 * Pass url param to encode an URL
 * URL must contain 'Modules.php?modname='
 *
 * @uses https://github.com/endroid/qr-code version 1.9.3 (PHP5)
 *
 * @example <img src="http://localhost/gitlab/rosariosis/modules/Student_Pickup/qrcode_image.php?url=http%3A%2F%2Flocalhost%2Fgitlab%2Frosariosis%2FModules.php%3Fmodname%3DStudents%2FStudent.php%26student_id%3D1%26category_id%3D17" />
 */

use Endroid\QrCode\QrCode;

if ( empty( $_REQUEST['url'] )
    || ! filter_var( $_REQUEST['url'], FILTER_VALIDATE_URL )
    || mb_strpos( $_REQUEST['url'], 'Modules.php?modname=' ) === false )
{
    die( 'Error: Not a valid URL' );
}

require_once 'vendor/autoload.php';

$qr_code = new QrCode();
$qr_code
    ->setText( $_REQUEST['url'] )
    ->setSize( 280 ) // 300x300 = 150x150 (Student Photo) scale 2x.
    ->setPadding( 10 )
    ->setErrorCorrection( 'high' )
    ->setForegroundColor( ['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0] )
    ->setBackgroundColor( ['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0] )
    ->setImageType( QrCode::IMAGE_TYPE_PNG )
;

// now we can directly output the qrcode
header( 'Content-Type: ' . $qr_code->getContentType() );
$qr_code->render();
