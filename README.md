Student Pickup module
=====================

![screenshot](https://gitlab.com/francoisjacquet/Student_Pickup/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/modules/student-pickup/

Version 1.2 - December, 2022

License GNU/GPLv2 or later

Author François Jacquet

Sponsored by @Hirama666, Lybia

DESCRIPTION
-----------
Student Pickup module for RosarioSIS. Scan the QR code presented by the parent and save the Drop-off (check-in) or Pickup (check-out) time for their child, along with whom brought or whom picked-up the child (Mother, Father or Other).
The module adds the "Drop-off & Pickup" tab to the _Student Info_ screen, for Teachers and Administrators (can edit), and for Parents and Students (can consult):

- Student Photo, Name, Grade Level and QR code (the QR code contains the link to this same screen)
- Drop-off & Pickup list for the selected dates

You can add fields and rename the tab from the _Students > Student Fields_ program.

Note: Only school days (see student's calendar) are available in the list.

Tip: You can print the QR codes for various students at once using the _Students > Print Student Info_ program.

Translated in [French](https://www.rosariosis.org/fr/modules/student-pickup/) and [Spanish](https://www.rosariosis.org/es/modules/student-pickup/).

WORKFLOW
--------
Here is the workflow:

- A parent (father, mother or other) presents the QR code to the person in charge of the students at the school gate.
- The person in charge scans the QR code with his smartphone.
- The Student Info program, "Drop-off & Pickup" tab for the child opens up in the browser.
- The person in charge can enter the (check-in or check-out) time and person in the list, then save.
- Then he can close the browser tab, let the student in (or out) and repeat with another parent.

CONTENT
-------
Students
- Student Info: Drop-off & Pickup tab

INSTALL
-------
Copy the `Student_Pickup/` folder (if named `Student_Pickup-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School > Configuration > Modules_ and click "Activate".

Requires RosarioSIS 9.0+
