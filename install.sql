/**
 * Install SQL
 * - Add program config options if any (to every schools)
 *
 * @package Student Pickup
 */

 -- Fix #102 error language "plpgsql" does not exist
 -- http://timmurphy.org/2011/08/27/create-language-if-it-doesnt-exist-in-postgresql/
 --
 -- Name: create_language_plpgsql(); Type: FUNCTION; Schema: public; Owner: postgres
 --

CREATE FUNCTION create_language_plpgsql()
RETURNS BOOLEAN AS $$
    CREATE LANGUAGE plpgsql;
    SELECT TRUE;
$$ LANGUAGE SQL;

SELECT CASE WHEN NOT (
    SELECT TRUE AS exists FROM pg_language
    WHERE lanname='plpgsql'
    UNION
    SELECT FALSE AS exists
    ORDER BY exists DESC
    LIMIT 1
) THEN
    create_language_plpgsql()
ELSE
    FALSE
END AS plpgsql_created;

DROP FUNCTION create_language_plpgsql();


/**
 * Student Field category (tab)
 * Include modules/Student_Pickup/Student.inc.php file.
 */
INSERT INTO student_field_categories
VALUES (nextval('student_field_categories_id_seq'), 'Drop-off & Pickup|en_US.utf8:Drop-off & Pickup|fr_FR.utf8:Accueil et sortie|es_ES.utf8:Recepción y salida', NULL, NULL, 'Student_Pickup/Student');

/**
 * Profile exceptions
 * Give access to tab to Admins and Teachers (Can Edit).
 * Give access to tab to Parents and Students (Can Use).
 */
INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '1','Students/Student.php&category_id='||currval('student_field_categories_id_seq'),'Y','Y';

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '2','Students/Student.php&category_id='||currval('student_field_categories_id_seq'),'Y','Y';

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '3','Students/Student.php&category_id='||currval('student_field_categories_id_seq'),'Y',NULL;

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '0','Students/Student.php&category_id='||currval('student_field_categories_id_seq'),'Y',NULL;


--
-- Name: student_pickup; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE OR REPLACE FUNCTION create_table_student_pickup() RETURNS void AS
$func$
BEGIN
    IF EXISTS (SELECT 1 FROM pg_catalog.pg_tables
        WHERE schemaname = CURRENT_SCHEMA()
        AND tablename = 'student_pickup') THEN
    RAISE NOTICE 'Table "student_pickup" already exists.';
    ELSE
        CREATE TABLE student_pickup (
            id serial PRIMARY KEY,
            student_id integer NOT NULL REFERENCES students(student_id),
            school_date date NOT NULL,
            dropoff_time time,
            dropoff_late_minutes integer,
            dropoff_person text,
            pickup_time time,
            pickup_late_minutes integer,
            pickup_person text,
            created_at timestamp DEFAULT current_timestamp,
            updated_at timestamp,
            UNIQUE(student_id, school_date)
        );

        CREATE TRIGGER set_updated_at
            BEFORE UPDATE ON student_pickup
            FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
    END IF;
END
$func$ LANGUAGE plpgsql;

SELECT create_table_student_pickup();
DROP FUNCTION create_table_student_pickup();
