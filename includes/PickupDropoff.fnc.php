<?php
/**
 * Pickup & Drop-off tab functions
 *
 * @package Student Pickup module
 */

if ( ! function_exists( 'StudentPickupListOutput' ) ) :
/**
 * Drop-off & Pickup ListOutput
 *
 * @param int    $student_id Student ID.
 * @param string $start_date Start Date.
 * @param string $end_date   End Date.
 */
function StudentPickupListOutput( $student_id, $start_date, $end_date )
{
	$table = 'student_pickup';

	$student_pickup = StudentPickupGet( $student_id, $start_date, $end_date );

	$functions = [
		'SCHOOL_DATE' => 'ProperDate',
		'DROPOFF_TIME' => 'StudentPickupMakeTime',
		'DROPOFF_PERSON' => 'StudentPickupMakePerson',
		'PICKUP_TIME' => 'StudentPickupMakeTime',
		'PICKUP_PERSON' => 'StudentPickupMakePerson',
	];

	$student_pickup_f = StudentPickupListFormat( $student_id, $student_pickup, $functions );

	$columns = [
		'SCHOOL_DATE' => _( 'Date' ),
		'DROPOFF_TIME' => dgettext( 'Student_Pickup', 'Drop-off' ),
		'DROPOFF_PERSON' => dgettext( 'Student_Pickup', 'Person' ),
		'PICKUP_TIME' => dgettext( 'Student_Pickup', 'Pickup' ),
		'PICKUP_PERSON' => dgettext( 'Student_Pickup', 'Person' ),
	];

	ListOutput(
		$student_pickup_f ,
		$columns,
		_( 'Day' ),
		_( 'Days' ),
		[],
		[],
		[ 'search' => false, 'valign-middle' => true ]
	);
}
endif;

/**
 * Drop-off & Pickup Get from DB + fill in for missing school dates
 *
 * @param int    $student_id Student ID.
 * @param string $start_date Start Date.
 * @param string $end_date   End Date.
 *
 * @return array Pickup array from DB + fill in for missing school dates
 */
function StudentPickupGet( $student_id, $start_date, $end_date )
{
	$student_pickup_RET = DBGet( "SELECT ID,STUDENT_ID,SCHOOL_DATE,
		DROPOFF_TIME,DROPOFF_LATE_MINUTES,DROPOFF_PERSON,
		PICKUP_TIME,PICKUP_LATE_MINUTES,PICKUP_PERSON,
        CREATED_AT,UPDATED_AT
        FROM student_pickup
        WHERE STUDENT_ID='" . (int) $student_id . "'
        AND SCHOOL_DATE BETWEEN '" . $start_date . "' AND '" . $end_date . "'
        ORDER BY SCHOOL_DATE",
        [],
        [ 'SCHOOL_DATE' ] );

	$current = strtotime( $start_date );
	$end = strtotime( $end_date );

	$student_pickup = [];

	while ( $current <= $end )
	{
		$current_date = date( 'Y-m-d', $current );

		$current = strtotime( '+1 days', $current );

		$is_school_day = DBGetOne( "SELECT 1
			FROM attendance_calendar
			WHERE SCHOOL_ID='" . UserSchool() . "'
			AND SYEAR='" . UserSyear() . "'
			AND CALENDAR_ID=(SELECT CALENDAR_ID
				FROM student_enrollment
				WHERE STUDENT_ID='" . (int) $student_id . "'
				AND SCHOOL_ID='" . UserSchool() . "'
				AND SYEAR='" . UserSyear() . "'
				AND START_DATE<='" . $current_date . "'
				AND (END_DATE IS NULL OR END_DATE>='" . $current_date . "')
				LIMIT 1)
			AND SCHOOL_DATE='" . $current_date . "'" );

		if ( ! $is_school_day
			&& empty( $student_pickup_RET[ $current_date ] ) )
		{
			// Skip if day is not a school day in student's Calendar.
			continue;
		}

		$student_pickup[ $current_date ] = [];

		if ( ! empty( $student_pickup_RET[ $current_date ] ) )
		{
			$student_pickup[ $current_date ] = $student_pickup_RET[ $current_date ][1];
		}
	}

	return $student_pickup;
}


/**
 * Format Student Pickup List
 * Using callback functions for each column
 *
 * @param int   $student_id     Student ID.
 * @param array $student_pickup Student Pickup rows from DB or empty row if school day not saved in DB.
 * @param array $functions      Callback functions, column name in key.
 *
 * @return array Formatted array.
 */
function StudentPickupListFormat( $student_id, $student_pickup, $functions )
{
	global $THIS_RET;

	if ( ! $functions )
	{
		return $student_pickup;
	}

	$student_pickup_f = [];

	$i = 1;

	foreach ( $student_pickup as $date => $pickup )
	{
		if ( empty( $pickup['SCHOOL_DATE'] ) )
		{
			$pickup['SCHOOL_DATE'] = $date;
		}

		if ( empty( $pickup['STUDENT_ID'] ) )
		{
			$pickup['STUDENT_ID'] = $student_id;
		}

		$THIS_RET = $pickup;

		foreach ( $functions as $column => $function )
		{
			if ( ! function_exists( $function ) )
			{
				continue;
			}

			if ( ! isset( $pickup[ $column ] ) )
			{
				$pickup[ $column ] = $function( null, $column );

				continue;
			}

			$pickup[ $column ] = $function( $pickup[ $column ], $column );
		}

		$student_pickup_f[ $i++ ] = $pickup;
	}

	return $student_pickup_f;
}

/**
 * Format Pickup/Drop-off time value for display
 *
 * @deprecated since RosarioSIS 11.5 use ProperTime()
 *
 * @param string $time Time value from DB.
 *
 * @return string Formatted time.
 */
function StudentPickupFormatTime( $time )
{
	if ( ! $time )
	{
		return $time;
	}

	// Preferred time based on locale.
	$time = strftime_compat( '%X', strtotime( DBDate() . ' ' . $time ) );

	// Strip seconds :00.
	$time = mb_substr( $time, -3 ) === ':00' ? mb_substr( $time, 0, -3 ) : $time;

	// Strip seconds when AM/PM :00.
	// 0x202f is hex for "narrow no-break space" unicode character.
	$time = str_replace( [ ':00 ', ':00 ' ], [ ' ', ' ' ], $time );

	return $time;
}

/**
 * Make Pickup/Drop-off time input
 * Callback function.
 *
 * @param string $value  Time value.
 * @param string $column DROPOFF_TIME or PICKUP_TIME.
 *
 * @return string Pickup/Drop-off time input.
 */
function StudentPickupMakeTime( $value, $column = 'DROPOFF_TIME' )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : $THIS_RET['SCHOOL_DATE'];

	if ( $value )
	{
		$value = [
			// Strip seconds :00.
			mb_substr( $value, -3 ) === ':00' ? mb_substr( $value, 0, -3 ) : $value,
			StudentPickupFormatTime( $value )
		];
	}

	return TextInput(
		$value,
		'values[' . $id . '][' . $column . ']',
		'',
		'type="time"'
	);
}

/**
 * Make Person input (auto-select)
 * Select will become a text input when "Other" is selected.
 * Callback function.
 *
 * @param string $value  Person value.
 * @param string $column DROPOFF_PERSON or PICKUP_PERSON.
 *
 * @return string Person input.
 */
function StudentPickupMakePerson( $value, $column = 'DROPOFF_PERSON' )
{
	global $THIS_RET;

	static $js_included = false;

	static $person_options = [];

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : $THIS_RET['SCHOOL_DATE'];

	if ( empty( $person_options[ $column ] ) )
	{
		$person_options[ $column ] = [
			'Mother' => dgettext( 'Student_Pickup', 'Mother' ),
			'Father' => dgettext( 'Student_Pickup', 'Father' ),
			'---' => _( 'Other' ),
		];

		// Custom options.
		$custom_options = DBGet( "SELECT DISTINCT " . DBEscapeIdentifier( $column ) . "
			FROM student_pickup
			WHERE STUDENT_ID='" . $THIS_RET['STUDENT_ID'] . "'
			AND " . DBEscapeIdentifier( $column ) . " IS NOT NULL
			AND " . DBEscapeIdentifier( $column ) . " NOT IN('Mother','Father','---')" );

		foreach ( (array) $custom_options as $custom_option )
		{
			$option = $custom_option[ $column ];

			$person_options[ $column ][ $option ] = $option;
		}
	}

	$input_name = 'values[' . $id . '][' . $column . ']';

	$options = 'maxlength=50';

	if ( $value === '---' )
	{
		// New option.
		return TextInput(
			$value === '---' ? [ '---', '<span style="color:red">-' . _( 'Edit' ) . '-</span>' ] : $value,
			$input_name,
			'',
			$options . ' style="width:128px"'
		);
	}

	// When Other option selected, change the auto pull-downs to text field.
	$return = '';

	if ( AllowEdit()
		&& ! isset( $_REQUEST['_ROSARIO_PDF'] )
		&& ! $js_included )
	{
		$js_included = true;

		ob_start();?>
		<script>
		function StudentPickupMaybeEditTextInput(el) {

			// Other option's value is ---.
			if ( el.value === '---' ) {

				var $el = $( el );

				// Remove parent <div> if any
				if ( $el.parent('div').length ) {
					$el.unwrap();
				}
				// Remove the select input.
				$el.remove();

				// Show & enable the text input of the same name.
				$( '[name="' + el.name + '_text"]' ).prop('name', el.name).prop('disabled', false).show().focus();
			}
		}
		</script>
		<?php $return = ob_get_clean();
	}

	if ( AllowEdit()
		&& ! isset( $_REQUEST['_ROSARIO_PDF'] ) )
	{
		// Add hidden & disabled Text input in case user chooses Other.
		$return .= TextInput(
			'',
			$input_name . '_text',
			'',
			$options . ' disabled style="display:none; width:128px"',
			false
		);
	}

	$return .= SelectInput(
		$value,
		$input_name,
		'',
		$person_options[ $column ],
		'N/A',
		'onchange="StudentPickupMaybeEditTextInput(this);" style="width:128px"'
	);

	return $return;
}

if ( ! function_exists( 'StudentPickupQRCodeImageURL' ) ) :
/**
 * QR code image URL to use inside <img src""> attribute.
 * QR code will contain link to the Student Info program, Drop-off & Pickup tab.
 *
 * @param int $student_id  Student ID.
 * @param int $category_id Student Info tab ID.
 *
 * @return string QR code image URL.
 */
function StudentPickupQRCodeImageURL( $student_id, $category_id )
{
	if ( function_exists( 'RosarioURL' ) )
	{
		$site_url = RosarioURL();
	}
	else
	{
		$site_url = 'http://';

		if ( ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] === 'on' )
			|| ( isset( $_SERVER['HTTP_X_FORWARDED_PROTO'] ) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' )
			|| ( isset( $_SERVER['HTTP_X_FORWARDED_SSL'] ) && $_SERVER['HTTP_X_FORWARDED_SSL'] === 'on' ) )
		{
			// Fix detect https inside Docker or behind reverse proxy.
			$site_url = 'https://';
		}

		$site_url .= $_SERVER['SERVER_NAME'];

		if ( $_SERVER['SERVER_PORT'] != '80'
			&& $_SERVER['SERVER_PORT'] != '443' )
		{
			$site_url .= ':' . $_SERVER['SERVER_PORT'];
		}

		$site_url .= dirname( $_SERVER['SCRIPT_NAME'] ) === DIRECTORY_SEPARATOR ?
			// Add trailing slash.
			'/' : dirname( $_SERVER['SCRIPT_NAME'] ) . '/';
	}

	$qr_code_url = $site_url . 'Modules.php?modname=Students/Student.php&student_id=' . $student_id . '&category_id=' . $category_id;

	$qr_code_image_url = $site_url . 'modules/Student_Pickup/qrcode_image.php?url=' . urlencode( $qr_code_url );

	return $qr_code_image_url;
}
endif;
