/**
 * Install MySQL
 * - Add program config options if any (to every schools)
 *
 * @package Student Pickup
 */

/**
 * Student Field category (tab)
 * Include modules/Student_Pickup/Student.inc.php file.
 */
INSERT INTO student_field_categories
VALUES (NULL, 'Drop-off & Pickup|en_US.utf8:Drop-off & Pickup|fr_FR.utf8:Accueil et sortie|es_ES.utf8:Recepción y salida', NULL, NULL, 'Student_Pickup/Student', NULL, NULL);

SELECT LAST_INSERT_ID() INTO @sfc_id;

/**
 * Profile exceptions
 * Give access to tab to Admins and Teachers (Can Edit).
 * Give access to tab to Parents and Students (Can Use).
 */
INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '1',CONCAT('Students/Student.php&category_id=', @sfc_id),'Y','Y';

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '2',CONCAT('Students/Student.php&category_id=', @sfc_id),'Y','Y';

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '3',CONCAT('Students/Student.php&category_id=', @sfc_id),'Y',NULL;

INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
SELECT '0',CONCAT('Students/Student.php&category_id=', @sfc_id),'Y',NULL;


--
-- Name: student_pickup; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE IF NOT EXISTS student_pickup (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    student_id integer NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    school_date date NOT NULL,
    dropoff_time time,
    dropoff_late_minutes integer,
    dropoff_person text,
    pickup_time time,
    pickup_late_minutes integer,
    pickup_person text,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp,
    UNIQUE(student_id, school_date)
);
